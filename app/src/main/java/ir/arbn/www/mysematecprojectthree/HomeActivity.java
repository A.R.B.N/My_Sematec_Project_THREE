package ir.arbn.www.mysematecprojectthree;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {
    EditText Name;
    EditText Family;
    Context mContext = HomeActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Name = findViewById(R.id.Name);
        Family = findViewById(R.id.Family);
        Name.setText(getShared("Name","Guest"));
        Family.setText(getShared("Family","Guest"));

        findViewById(R.id.Save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveShared("Name", Name.getText().toString());
                saveShared("Family", Family.getText().toString());
                Toast.makeText(mContext, "Your Data Has Been Saved", Toast.LENGTH_SHORT).show();
                Name.setText("");
                Family.setText("");
            }
        });
    }

    void saveShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();
    }

    String getShared(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, defValue);
    }
}
